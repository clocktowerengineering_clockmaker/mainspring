import typing
from PyQt5.QtCore import  *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import PyTango
import os
import json
import traceback
from sequencer import SequenceValidator

from PyQt5.QtCore import Qt, QModelIndex, QVariant, QAbstractItemModel

import sys

data = [0,0,0,0,0,0,0,0,0]

class customModel(QAbstractItemModel):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.data = data
        self.header_data = ['Note','type','value','address','attribute','criteriaType','jumpTo','compareaddress','compareattribute']

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()

        item = index.internalPointer()
        column = index.column()

        if role == Qt.DisplayRole:
            return item.data[column]
        elif role == Qt.EditRole:
            return item.data[column]  # You can return editable data here if needed
        else:
            return QVariant()
        
    def index(self, row, column, parent=QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QModelIndex()
        
        if not parent.isValid():
            parent_item = self.data
        else:
            parent_item = parent.internalPointer()

        if row < 0 or row >= len(parent_item.children):
            return QModelIndex()
        
        child_item = parent_item.children[row]
            
        if column < 0 or column >= len(child_item.data):
            return QModelIndex()

        return self.createIndex(row, column, child_item)

    def parent(self, index):
        if not index.isValid():
            return QModelIndex()

        item = index.internalPointer()
        parent_item = item.parent

        if parent_item == self.data:
            return QModelIndex()

        row = parent_item.children.index(item)
        return self.createIndex(row, 0, parent_item)
    
    def rowCount(self, parent=QModelIndex()):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parent_item = self.data
        else:
            parent_item = parent.internalPointer()

        return len(parent_item.children) 

    def columnCount(self, parent=QModelIndex()):
        #return len(self.data.data) if not parent.isValid() else 0
        return 9
    
    def dropMimeData(self, data, action, row, col, parent):
        """
        Always move the entire row, and don't allow column "shifting"
        """
        response = super().dropMimeData(data, Qt.CopyAction, row, 0, parent)
        
        for i in range(int(self.rowCount())):
            
            item = self.item(i,1)
            item.setEnabled(False) # Hack / Fixes copying instead of moving when style column is disabled
        
        return response

    def mimeData(self,indices):
        """
        Move all data, including hidden/disabled columns
        """
        index = indices

        new_data = []
        self.rowLength = int(round(len(index)/9,1))
    
        for i in range(len(indices)):
            index = indices[i]
            
            for col in range(self.columnCount()):
                
                new_data.append(index.sibling(index.row(),col))        

            item = self.item(index.row(),1)
            self.was_enabled = item.isEnabled()
            
            if self.moveMode:

                item.setEnabled(True) # Hack// Fixes copying instead of moving when item is disabled
            
            else:

                item.setEnabled(False)

        return super().mimeData(new_data) 

    def RenumberRows(self):

        for index in range(self.rowCount()):

            self.setItem(index,0,QtGui.QStandardItem(""))    
    
    def headerData(self, section, orientation, role):
        if role == Qt.ItemDataRole.DisplayRole and orientation == Qt.Orientation.Horizontal:
            return self.header_data[section]
        return None    

    def setHeaderData(self, section, orientation, value, role):
        if role == Qt.ItemDataRole.DisplayRole and orientation == Qt.Orientation.Horizontal and 0 <= section < len(self.header_data):
            self.header_data[section] = value
            self.headerDataChanged.emit(orientation, section, section)
            return True
        return False
    
    def appendRow(self, rows, parent=QModelIndex()):
        self.beginInsertRows(parent, len(self.data), len(self.data) + len(rows) - 1)
        parentItem = parent.internalPointer() if parent.isValid() else self.data
        parentItem.children.extend(rows)
        self.endInsertRows()



class recipeSteps(QtWidgets.QTableView):
    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.moveMode = False
        self.stepType = "criteria"
        self.setSelectionBehavior(self.SelectRows) #Select whole rows
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection) 
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop) # Objects can only be drag/dropped internally and are moved instead of copied
        self.setDragDropOverwriteMode(False) # Removes the original item after moving instead of clearing it
        self.setAlternatingRowColors(True)
        #self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        # Set our custom style - this draws the drop indicator across the whole row
        self.HorizontalLabels = ['Note','type','value','address','attribute','criteriaType','jumpTo','compareaddress','compareattribute']
        self.setStyle(customStyle())
        model = customModel()
        #model.setHorizontalHeaderLabels(self.HorizontalLabels)
        for i in range(len(self.HorizontalLabels)):
            model.setHeaderData(i, Qt.Orientation.Horizontal,self.HorizontalLabels[i],Qt.ItemDataRole.DisplayRole)
        self.setModel(model)
        # self.testpopulate()

    def DumpSteps(self):
        
        model = self.model()
        tableDict = {}
        for row_index in range(model.rowCount()):
            stepDict = {}
            stepItem = {}
            for column_index in reversed(range(model.columnCount())):

                item = model.data(model.index(row_index,column_index))
                itemDict = {}
                if column_index !=2:
                    itemDict[str(self.HorizontalLabels[column_index])] = str(item)
                elif str(item)=='':
                    itemDict[str(self.HorizontalLabels[column_index])] = str(item)
                else:
                    itemDict[str(self.HorizontalLabels[column_index])] = str(item)
            
                stepDict.update(itemDict)
            stepItem[str(row_index+1)] = stepDict
            tableDict.update(stepItem)

        #stepsDict = {}
        #stepsDict[str("Steps")] = int(model.rowCount())
        #tableDict.update(stepsDict)
        
        return tableDict

    def PopulateJSON(self,data):

        model = self.model()
        
        # loop through each "row"
        try:
        
            for rowindex in range(1,len(data)+1):
                rowdata = []
                for column in range(len(self.HorizontalLabels)):

                    
                    rowdata.append(QtGui.QStandardItem(str(data[str(rowindex)][str(self.HorizontalLabels[column])])))

                
                model.appendRow(rowdata)
        except:
            traceback.print_exc()

    def testpopulate(self):
        
        model = self.model()
        for row in ['1','2','3']:
            data = []
            for column in range(len(self.HorizontalLabels)):
                if column == 0:
                    item=QtGui.QStandardItem("")

                else:
                    colname = str(chr(65+column))
                    item = QtGui.QStandardItem(f'{colname}{row}')
                item.setDropEnabled(False)

                if column !=0:
                    #item.setEnabled(set_enabled)
                    pass

                data.append(item)

            model.appendRow(data)

    def moveCopyMode(self):

        if self.moveMode:
            self.moveMode = False
            self.setDragDropMode(self.DragDrop)
            #self.setDragDropOverwriteMode(False)
            
        else:
            
            self.setDragDropMode(self.InternalMove)
            self.moveMode = True
            #self.setDragDropOverwriteMode(True)
            #self.setDefaultDropAction(QtCore.Qt.MoveAction)
        
        self.model().moveMode = self.moveMode
        
    def DeleteRows(self):
        index_list = []
        model = self.model()
        for model_index in self.selectionModel().selectedRows():
            index = QtCore.QPersistentModelIndex(model_index)
            index_list.append(index)

        for index in index_list:
            model.removeRow(index.row())

    def DeleteAllRows(self):
        model = self.model()
        model.setRowCount(0)

    def AddRow(self):
        model = self.model()
        data = []

        # TODO: let's make this so the only cells that are enabled are the ones that should be for this device type.

        # TODO: make criteria only of certain types. https://stackoverflow.com/questions/30457935/pyqt4-adding-combobox-in-qtableview

        if str(self.stepType) == "Durationx":
            for column in range(len(self.HorizontalLabels)):           

                if column == 0:
                    item = QtGui.QStandardItem("")

                elif column ==1:
                    
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)

                elif column ==2:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                else:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                item.setDropEnabled(False)
                data.append(item)

        elif str(self.stepType) == "Parameterx":
            for column in range(len(self.HorizontalLabels)):           

                if column == 0:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column ==1:
                    
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)

                elif column ==2:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 3:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 4:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 5:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                else:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                item.setDropEnabled(False)
                data.append(item)

        elif str(self.stepType) == "Criteriax":
            for column in range(len(self.HorizontalLabels)):           

                if column == 0:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column ==1:
                    
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)

                elif column ==2:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 3:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 4:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 5:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 6:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                else:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                item.setDropEnabled(False)
                data.append(item)

        if str(self.stepType) == "Jumpx":
            for column in range(len(self.HorizontalLabels)):           

                if column == 0:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column ==1:
                    
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)

                elif column ==2:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 3:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 4:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 5:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 6:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 7:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                else:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                item.setDropEnabled(False)
                data.append(item)

        else:
            for column in range(len(self.HorizontalLabels)):           

                if column !=1:
                    item = QtGui.QStandardItem("")
                    
                else:
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)
                item.setDropEnabled(False)
                data.append(item)

        model.appendRow(data)

    def AddAddressAndAttribute(self,deviceserverpath,attribute):
        model = self.model()
        device = QtGui.QStandardItem(str(deviceserverpath))
        attr = QtGui.QStandardItem(str(attribute))
        rows = sorted(set(index.row() for index in
                      self.selectionModel().selectedIndexes()))
        for row in rows:
        
            model.setItem(row,4,device)
            model.setItem(row,5,attr)

    def AddCompareAddressAndAttribute(self,deviceserverpath,attribute):

        model = self.model()
        device = QtGui.QStandardItem(str(deviceserverpath))
        attr = QtGui.QStandardItem(str(attribute))
        rows = sorted(set(index.row() for index in
                      self.selectionModel().selectedIndexes()))
        for row in rows:
        
            model.setItem(row,8,device)
            model.setItem(row,9,attr)
        
        
class attributeList(QtWidgets.QTableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.moveMode = False
        self.setSelectionBehavior(self.SelectRows) #Select whole rows
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setDragEnabled(False)
        self.setAlternatingRowColors(True)
        
        model = QtGui.QStandardItemModel()
        model.setHorizontalHeaderLabels(['Attribute'])

        self.setModel(model)
    
    def DeleteAllRows(self):
        model = self.model()
        model.setRowCount(0)
        

    def AddRow(self,text):
        model = self.model()
        data = []
        item = QtGui.QStandardItem(str(text))
        item.setEnabled(True)
        item.setDropEnabled(False)
        data.append(item)       
        model.appendRow(data)

  


class moveCopyMode(QtWidgets.QPushButton):
    def __init__(self,parent=None):
        super().__init__(parent)

        self.CopyMode = False

    def clicktext(self):
        if self.CopyMode:

            self.CopyMode = False
            self.setText("Copy")
        
        else:
            self.CopyMode = True
            self.setText("Move")

class customStyle(QtWidgets.QProxyStyle):
    def drawPrimitive(self, element, option, painter, widget=None):
        """
        Draw a line across the entire row rather than just the column
        we're hovering over.  This may not always work depending on global
        style - for instance I think it won't work on OSX.
        """
        if element == self.PE_IndicatorItemViewItemDrop and not option.rect.isNull():
            option_new = QtWidgets.QStyleOption(option)
            option_new.rect.setLeft(0)
            if widget:
                option_new.rect.setRight(widget.width())
            option = option_new
        super().drawPrimitive(element, option, painter, widget)

class Ui_MainSpringGUI(QWidget):
    

    def setupUi(self, MainSpringGUI):
        MainSpringGUI.setObjectName("MainSpringGUI")
        MainSpringGUI.setEnabled(True)
        self.recipeSteps = recipeSteps(MainSpringGUI)
        self.recipeSteps.setGeometry(QtCore.QRect(10, 10, 1051, 571))
        self.recipeSteps.setDefaultDropAction(QtCore.Qt.CopyAction)

        self.DeviceProxyAddress = "raako/handlers/sequencer"    
        # add undoview later, this isn't important right this moment

        #self.undoView = QtWidgets.QUndoView(MainSpringGUI)
        #self.undoView.setGeometry(QtCore.QRect(1090, 480, 221, 181))
        #self.undoView.setObjectName("undoView")

        self.AddStep = QtWidgets.QPushButton(MainSpringGUI)
        self.AddStep.setGeometry(QtCore.QRect(140, 600, 75, 23))
        self.AddStep.setObjectName("AddStep")
        self.AddStep.clicked.connect(self.CriteriaType)
        self.comboBox = QtWidgets.QComboBox(MainSpringGUI)
        self.comboBox.setGeometry(QtCore.QRect(10, 600, 121, 22))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.RemoveStep = QtWidgets.QPushButton(MainSpringGUI)
        self.RemoveStep.setGeometry(QtCore.QRect(140, 630, 100, 23))
        self.RemoveStep.setObjectName("RemoveStep")
        self.RemoveStep.clicked.connect(self.recipeSteps.DeleteRows)

        self.SequencerButton = QtWidgets.QPushButton(MainSpringGUI)
        self.SequencerButton.setGeometry(QtCore.QRect(340, 630, 100, 23))
        self.SequencerButton.setObjectName("SendToSequencer")
        self.SequencerButton.clicked.connect(self.SendToSequencer)

        
        self.deviceTreeView = QtWidgets.QTreeWidget(MainSpringGUI)
        self.deviceTreeView.setGeometry(QtCore.QRect(1090, 10, 221, 250))
        self.deviceTreeView.setObjectName("deviceTreeView")
        self.deviceTreeView.setHeaderLabel("Device Servers")
        self.deviceTreeView.setDragEnabled(False)
        self.deviceTreeView.setAlternatingRowColors(True)
        self.buildDeviceTreeView(self.getDeviceServers(),parent=self.deviceTreeView)
        self.deviceTreeView.itemClicked.connect(self.treeClickHandler)
        
        self.attributeListView = attributeList(MainSpringGUI)
        self.attributeListView.setGeometry(1090, 280,221,250)
        self.attributeListView.setDragEnabled(False)
        self.attributeListView.setStyle(customStyle())
        self.attributeListView.clicked.connect(self.attributeListHandler)
        
        self.MoveCopyMode = moveCopyMode(MainSpringGUI)
        self.MoveCopyMode.setGeometry(QtCore.QRect(240, 600, 75, 23))
        self.MoveCopyMode.setAutoDefault(False)
        self.MoveCopyMode.setObjectName("MoveCopyMode")
        self.MoveCopyMode.clicked.connect(self.MoveCopyMode.clicktext)
        self.MoveCopyMode.clicked.connect(self.recipeSteps.moveCopyMode)
        self.Append = QtWidgets.QPushButton(MainSpringGUI)
        self.Append.setGeometry(QtCore.QRect(340, 600, 75, 23))
        self.Append.setAutoDefault(False)
        self.Append.setObjectName("Append")
        self.Append.clicked.connect(self.append)
        self.SaveSteps = QtWidgets.QPushButton(MainSpringGUI)
        self.SaveSteps.setGeometry(QtCore.QRect(440, 600, 75, 23))
        self.SaveSteps.setAutoDefault(False)
        self.SaveSteps.setObjectName("SaveSteps")
        self.SaveSteps.clicked.connect(self.save)
        self.LoadSteps = QtWidgets.QPushButton(MainSpringGUI)
        self.LoadSteps.setGeometry(QtCore.QRect(540, 600, 75, 23))
        self.LoadSteps.setAutoDefault(False)
        self.LoadSteps.setObjectName("LoadSteps")
        self.LoadSteps.clicked.connect(self.open)
        self.AddAttribute = QtWidgets.QPushButton(MainSpringGUI)
        self.AddAttribute.setGeometry(QtCore.QRect(1090, 540,221,23))
        self.AddAttribute.setAutoDefault(False)
        self.AddAttribute.setObjectName("AddAttribute")
        self.AddAttribute.clicked.connect(self.AddAttributeToStep)
        self.AddAttribute.setDisabled(True)
        self.AddCriteria = QtWidgets.QPushButton(MainSpringGUI)
        self.AddCriteria.setGeometry(QtCore.QRect(1090, 570,221,23))
        self.AddCriteria.setAutoDefault(False)
        self.AddCriteria.setObjectName("AddAttribute")
        self.AddCriteria.clicked.connect(self.AddCriteriaToStep)
        self.AddCriteria.setDisabled(True)

        self.SequenceValidator = SequenceValidator.SequenceValidator()

        self.retranslateUi(MainSpringGUI)
        QtCore.QMetaObject.connectSlotsByName(MainSpringGUI)

    def retranslateUi(self, MainSpringGUI):
        _translate = QtCore.QCoreApplication.translate
        MainSpringGUI.setWindowTitle(_translate("MainSpringGUI", "MainSpringGUI"))
        self.AddStep.setText(_translate("MainSpringGUI", "Add Step"))
        self.comboBox.setItemText(0, _translate("MainSpringGUI", "criteria"))
        self.comboBox.setItemText(1, _translate("MainSpringGUI", "parameter"))
        self.comboBox.setItemText(2, _translate("MainSpringGUI", "duration"))
        self.comboBox.setItemText(3, _translate("MainSpringGUI", "jump"))
        self.comboBox.setItemText(4, _translate("MainSpringGUI", "jumpIf"))
        self.comboBox.setItemText(5, _translate("MainSpringGUI", "jumpCompare"))
        self.RemoveStep.setText(_translate("MainSpringGUI", "Remove Step"))
        self.MoveCopyMode.setText(_translate("MainSpringGUI", "Copy"))
        self.Append.setText(_translate("MainSpringGUI", "Append"))
        self.SaveSteps.setText(_translate("MainSpringGUI", "Save"))
        self.LoadSteps.setText(_translate("MainSpringGUI", "Load"))
        self.AddAttribute.setText(_translate("MainSpringGUI", "AddAttribute"))
        self.AddCriteria.setText(_translate("MainSpringGUI", "AddCriteria"))
        self.SequencerButton.setText(_translate("MainSpringGUI", "SendToSequencer"))

    def treeClickHandler(self,item,column_number):

        deviceserverpathparts = []
        
        # this basically traverses the tree and grabs all the parents, then reverses it so the text is in the right order.
        while item is not None:
            deviceserverpathparts.append(item.text(0))
            item = item.parent()
        deviceserverpathparts.reverse()

        # generate the full path.
        deviceserverpath = '/'.join(deviceserverpathparts)
        # set for later.
        self.deviceserverpath = deviceserverpath
        
        # if there are 3 parts (ie address conforms with 1/2/3 address), grab attributes.
        if len(deviceserverpathparts)==3:
            attributes = PyTango.DeviceProxy(deviceserverpath).get_attribute_list()
            
            listattr = []
            # remove presently selected attributes.

            self.attributeListView.DeleteAllRows()
            
            # get all attributes and push them to the view in alphabetical order.
            for i in range(len(attributes)):
                listattr.append(str(attributes[i]))
            listattr.sort()            
            for i in range(len(listattr)):
                self.attributeListView.AddRow(str(listattr[i]))

    def attributeListHandler(self):
        
        index = self.attributeListView.selectionModel().currentIndex()
        attribute = index.sibling(index.row(),index.column()).data()
        self.attributetext = str(attribute)
        self.AddAttribute.setEnabled(True)
        self.AddCriteria.setEnabled(True)

    def SendToSequencer(self):
        try:
            tableDict = self.recipeSteps.DumpSteps()
            tableString = str(json.dumps(tableDict))
            validationcode, validationstring = self.SequenceValidator.ValidateSequence(tableString)
            if validationcode >=0:
                SequencerDS = PyTango.DeviceProxy(self.DeviceProxyAddress)
                SequencerDS.LoadSequence(tableString)
            else:
                msg = QMessageBox()
                msg.setWindowTitle("Error!")
                msg.setText("Validation failed with code " + str(validationcode) + "\r\n" + str(validationstring))
                x = msg.exec_()
        except:
            traceback.print_exc()

    def AddAttributeToStep(self):

        self.recipeSteps.AddAddressAndAttribute(self.deviceserverpath,self.attributetext)

    def AddCriteriaToStep(self):

        self.recipeSteps.AddCompareAddressAndAttribute(self.deviceserverpath,self.attributetext)
            

    def CriteriaType(self):
        
        # sets the criteria based on the incoming value.
        self.recipeSteps.stepType = str(self.comboBox.currentText())
        self.recipeSteps.AddRow()

    def open(self,append=False):
        
        path = QtWidgets.QFileDialog.getOpenFileName(self, 'Open a file', os.path.expanduser('~'),'All Files (*.*)')
        
        if path != ('', ''):
            
            
            file = open(path[0])
            validationstep = False
            try:
                jsondata = json.load(file)
                validationstep = True
                validationcode, validationstring = self.SequenceValidator.ValidateSequence(json.dumps(jsondata))

                
                

            except:
                msg = QMessageBox()
                msg.setWindowTitle("Error!")
                if validationstep:
                    msg.setText("Validation failed with code " + str(validationcode) + "\r\n" + str(validationstring))
                else:
                    msg.setText("Bad JSON on load! File is corrupt.")
                x = msg.exec_()
            if not append:
                self.recipeSteps.DeleteAllRows()
            self.recipeSteps.PopulateJSON(jsondata)

        
    
    def append(self):
        
        self.open(append=True)
            
        
    def save(self):

        tableDict = self.recipeSteps.DumpSteps()
        validationcode, validationstring = self.SequenceValidator.ValidateSequence(str(json.dumps(tableDict)))
        if validationcode >=0:
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.DontUseNativeDialog
            fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self,"Save Sequencer File",os.path.expanduser('~'),"JSON Files (*.json)", options=options)
            if fileName:
                if ".json" not in fileName:
                    fileName = fileName + ".json"
                print(fileName)
                with open(fileName,'w') as outputfile:
                    json.dump(tableDict,outputfile)
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Error!")
            msg.setText("Validation failed with code " + str(validationcode) + "\r\n" + str(validationstring))
            x = msg.exec_()



    def getDeviceServers(self):
        
        # grab exported device servers.
        deviceservers =  deviceservers = PyTango.Database().get_device_exported('*').value_string     
        # sort into a nested dict.
        tree = {} 
        # for each path
        for path in deviceservers:                
            # start at the top
            node = tree       
            # split into a list
            for level in path.split('/'): 
                # split the path into a list 
                if level:                 
                    # if a name is non-empty, move to the deeper level or create if nonexistent
                    node = node.setdefault(level, dict()) 

        
        return tree
        
    def buildDeviceTreeView(self,data=None, parent=None):

        for key,value in data.items():

            item = QTreeWidgetItem(parent)
            item.setText(0,key)
            if isinstance(value,dict):
                self.buildDeviceTreeView(data=value,parent=item)

    


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    MainSpringGUI = QtWidgets.QWidget()
    ui = Ui_MainSpringGUI()
    ui.setupUi(MainSpringGUI)
    MainSpringGUI.show()
    sys.exit(app.exec_())