from typing import Deque
from PyQt5.QtCore import  *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets

import PyTango
import os
import json
import traceback
import re
from sequencer import SequenceValidator

class customModel(QtGui.QStandardItemModel):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.moveMode = False
        self.rowLength = 0
        self.was_enabled = False
        self.modelIndex = QModelIndex()
        self.indexesMimed = None #Type: QModelIndex List

    def data(self, index, role):
        return QStandardItemModel.data(self, index, role)

    def setData(self, index, value, role):
        oldData = index.data(role)

        if role == Qt.ItemDataRole.EditRole:

            command = commandSetData(ui.recipeSteps.modelmodel, index, value, oldData)
            proxy.mStack.push(command)
            self.itemChanged.emit(self.itemFromIndex(index))
            self.changeTextColor(index, role)

        return False
    
    def handleClicked(self, index):
        # Simulate data change when a cell is clicked
        self.setData(index, "New Value", Qt.ItemDataRole.EditRole)

    def dropMimeData(self, data, action, row, col, parent):
        """
        Always move the entire row, and don't allow column "shifting"
        """

        command = commandDropMimeData(data, action, row, col, parent)
        proxy.mStack.push(command)

        return True
        
    
    def mimeData(self,indices):
        """
        Move all data, including hidden/disabled columns
        """
        self.indexesMimed = indices

        new_data = []
        self.rowLength = int(round(len(self.indexesMimed)/9,1))
        for i in range(len(indices)):
            index = indices[i]
            
            for col in range(self.columnCount()):
                
                new_data.append(index.sibling(index.row(),col))        

            item = self.item(index.row(),1)
            self.was_enabled = item.isEnabled()
            
            if self.moveMode:

                item.setEnabled(True) # Hack// Fixes copying instead of moving when item is disabled
            
            else:

                item.setEnabled(False)

        return super().mimeData(new_data)

    def RenumberRows(self):

        for index in range(self.rowCount()):

            self.setItem(index,0,QtGui.QStandardItem(""))    

    def pathFromIndex(self,index):
        PathItem = QPair()
        iter = index
        path = []
        if (iter.isValid):
            PathItem.first = iter.row()
            PathItem.second = iter.column()
            path.insert(0, PathItem)
            iter = iter.parent()
        return path
    
    def pathToIndex(self, path, model):
        iter = ui.recipeSteps.modelmodel.modelIndex
 
        for i in range(len(path)):
            iter = model.index(path[i].first, path[i].second, iter)

        return iter
    
    def checkData(self, index, role = Qt.ItemDataRole.EditRole):
        value = str(QStandardItemModel.data(self, index, role))
        valueList = []
        valueList.append(value)
        if index.column() == 3 or index.column() == 7: #(compare)Address column
            doesMatch = bool(re.search("^(.+?)/(.+?)/(.+?)$", value))
            return doesMatch
        
        if index.column() == 4 or index.column() == 8: #(compare)Attribute column
            matchingSet = set(ui.allAttr) & set(valueList) #This should quickly find the matching set if it exists
            if len(matchingSet) > 0:
                return True
            else:
                return False

        
        return True
    
    def changeTextColor(self, index, role):
        dataIsValid = self.checkData(index, role)
        item = self.itemFromIndex(index)
        if dataIsValid == False:
            item.setForeground(QtCore.Qt.red)
        elif dataIsValid == True:
            item.setForeground(QtCore.Qt.black)

    



class recipeSteps(QtWidgets.QTableView):
    
    def __init__(self, parent=None):
        super().__init__(parent)
        self.moveMode = False
        self.stepType = "criteria"
        self.setSelectionBehavior(self.SelectRows) #Select whole rows
        self.dragEnabled = True
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection) 
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop) # Objects can only be drag/dropped internally and are moved instead of copied
        self.setDragDropOverwriteMode(False) # Removes the original item after moving instead of clearing it
        self.setAlternatingRowColors(True)
        #self.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        # Set our custom style - this draws the drop indicator across the whole row
        self.HorizontalLabels = ['Note','type','value','address','attribute','criteriaType','jumpTo','compareaddress','compareattribute']
        self.HorizontalLabelsWidths = [115, 90, 115, 115, 115, 115, 115, 115, 142]
        self.setStyle(customStyle())
        self.modelmodel = customModel()
        self.setItemDelegateForColumn(5, ComboBoxItemDelegate())
        self.modelmodel.setHorizontalHeaderLabels(self.HorizontalLabels)
        self.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.setModel(self.modelmodel)
        self.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)

        #This kind of works ok but not really
        #self.resizeColumnsToContents()
        #self.horizontalHeader().setStretchLastSection(True)

        for col in range(len(self.HorizontalLabels)):
            self.setColumnWidth(col, self.HorizontalLabelsWidths[col])

        # self.testpopulate()


    def DumpSteps(self):
        
        model = self.model()
        tableDict = {}
        for row_index in range(model.rowCount()):
            stepDict = {}
            stepItem = {}
            for column_index in reversed(range(model.columnCount())):

                item = model.data(model.index(row_index,column_index), Qt.ItemDataRole.EditRole)
                itemDict = {}
                if column_index !=2:
                    itemDict[str(self.HorizontalLabels[column_index])] = str(item)
                elif str(item)=='':
                    itemDict[str(self.HorizontalLabels[column_index])] = str(item)
                else:
                    itemDict[str(self.HorizontalLabels[column_index])] = str(item)
            
                stepDict.update(itemDict)
            stepItem[str(row_index+1)] = stepDict
            tableDict.update(stepItem)

        #stepsDict = {}
        #stepsDict[str("Steps")] = int(model.rowCount())
        #tableDict.update(stepsDict)
        
        return tableDict

    def PopulateJSON(self,data, undo):

        model = self.model()
        
        # loop through each "row"
        try:
        
            for rowindex in range(1,len(data)+1):
                rowdata = []
                if undo:
                    ui.recipeSteps.DeleteRows(rowindex)
                for column in range(len(self.HorizontalLabels)):

                    if not undo:
                        rowdata.append(QtGui.QStandardItem(str(data[str(rowindex)][str(self.HorizontalLabels[column])])))
                
                model.appendRow(rowdata)
        except:
            traceback.print_exc()
        return data

    def testpopulate(self):
        
        model = self.model()
        for row in ['1','2','3']:
            data = []
            for column in range(len(self.HorizontalLabels)):
                if column == 0:
                    item=QtGui.QStandardItem("")

                else:
                    colname = str(chr(65+column))
                    item = QtGui.QStandardItem(f'{colname}{row}')
                item.setDropEnabled(False)

                if column !=0:
                    #item.setEnabled(set_enabled)
                    pass

                data.append(item)

            model.appendRow(data)

    def moveCopyMode(self):

        if self.moveMode:
            self.moveMode = False
            self.setDragDropMode(self.DragDrop)
            #self.setDragDropOverwriteMode(False)
            
        else:
            
            self.setDragDropMode(self.InternalMove)
            self.moveMode = True
            #self.setDragDropOverwriteMode(True)
            #self.setDefaultDropAction(QtCore.Qt.MoveAction)
        
        self.model().moveMode = self.moveMode
        
    def DeleteRows(self, index, undo = False):
        command = commandDeleteRows()
        proxy.mStack.push(command)

    def DeleteAllRows(self):
        model = self.model()
        model.setRowCount(0)

    def AddRow(self):
        model = self.model()
        data = []

        # TODO: let's make this so the only cells that are enabled are the ones that should be for this device type.

        # TODO: make criteria only of certain types. https://stackoverflow.com/questions/30457935/pyqt4-adding-combobox-in-qtableview

        if str(self.stepType) == "Durationx":
            for column in range(len(self.HorizontalLabels)):           

                if column == 0:
                    item = QtGui.QStandardItem("")

                elif column ==1:
                    
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)

                elif column ==2:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                else:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                item.setDropEnabled(False)
                data.append(item)

        elif str(self.stepType) == "Parameterx":
            for column in range(len(self.HorizontalLabels)):           

                if column == 0:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column ==1:
                    
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)

                elif column ==2:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 3:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 4:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 5:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                else:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                item.setDropEnabled(False)
                data.append(item)

        elif str(self.stepType) == "Criteriax":
            for column in range(len(self.HorizontalLabels)):           

                if column == 0:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column ==1:
                    
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)

                elif column ==2:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 3:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 4:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 5:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column == 6:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                else:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                item.setDropEnabled(False)
                data.append(item)

        if str(self.stepType) == "Jumpx":
            for column in range(len(self.HorizontalLabels)):           

                if column == 0:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                elif column ==1:
                    
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)

                elif column ==2:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 3:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 4:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 5:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 6:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                elif column == 7:
                    item = QtGui.QStandardItem("")
                    item.setEnabled(True)

                else:
                    
                    item = QtGui.QStandardItem("")
                    item.setEnabled(False)

                item.setDropEnabled(False)
                data.append(item)

        else:
            for column in range(len(self.HorizontalLabels)):           

                if column !=1:
                    item = QtGui.QStandardItem("")
                    
                else:
                    item = QtGui.QStandardItem(str(self.stepType))
                    item.setEnabled(False)
                item.setDropEnabled(False)
                data.append(item)

        model.appendRow(data)

    def AddAddressAndAttribute(self,deviceserverpath,attribute):
        model = self.model()
        changedCells = [[], [], []]

        device = str(deviceserverpath) #Changing the QStandardItems to strings to allow multiple row copies
        attr = str(attribute)
        locationList = []

        index_list = []
        for model_index in ui.recipeSteps.selectionModel().selectedRows(): #grab QPersistentModelIndexes from user selection
            index = QtCore.QPersistentModelIndex(model_index)
            index_list.append(index)
        
        for index in index_list: #extract row and column data, then store as a list of QPairs 
            locationPair = QPair()
            locationPair.first = index.row()
            locationPair.second = index.column()

            locationList.append(locationPair)

        for location in range(len(locationList)): # iterate through list of QPairs and setItem, generating a new set of items each iteration to prevent duplicate error
            indexPair = QPair() # type: QModelIndex
            oldPair = QPair() # type: list of QStandardItems
            currentPair = QPair()

            row = locationList[location].first
            col = locationList[location].second

            device = QtGui.QStandardItem(str(deviceserverpath))
            attr = QtGui.QStandardItem(str(attribute))

            #Grab the current cell items
            oldPair.first = model.takeItem(row, 3)
            oldPair.second = model.takeItem(row, 4)

            #Set items to new value
            model.setItem(row, 3, device) 
            model.setItem(row, 4, attr)

            #Create an index of cells that were changed
            indexPair.first = model.createIndex(row, 3, model)
            indexPair.second = model.createIndex(row, 4, model)    

            #Grab the items at the locations that were changed
            currentPair.first = model.item(row, 3)
            currentPair.second = model.item(row, 4)

            #Generate list of lists of lists for return
            changedCells[0].append(indexPair)
            changedCells[1].append(oldPair)
            changedCells[2].append(currentPair)

        return changedCells

    def AddCompareAddressAndAttribute(self,deviceserverpath,attribute):
        model = self.model()
        changedCells = [[], [], []]

        device = str(deviceserverpath) #Changing the QStandardItems to strings to allow multiple row copies
        attr = str(attribute)
        locationList = []

        index_list = []
        for model_index in ui.recipeSteps.selectionModel().selectedRows(): #grab QPersistentModelIndexes from user selection
            index = QtCore.QPersistentModelIndex(model_index)
            index_list.append(index)
        
        for index in index_list: #extract row and column data, then store as a list of QPairs 
            locationPair = QPair()
            locationPair.first = index.row()
            locationPair.second = index.column()

            locationList.append(locationPair)

        for location in range(len(locationList)): # iterate through list of QPairs and setItem, generating a new set of items each iteration to prevent duplicate error
            indexPair = QPair() # type: QModelIndex
            oldPair = QPair() # type: list of QStandardItems
            currentPair = QPair() 

            row = locationList[location].first
            col = locationList[location].second

            device = QtGui.QStandardItem(str(deviceserverpath))
            attr = QtGui.QStandardItem(str(attribute))

            #Grab the current cell items
            oldPair.first = model.takeItem(row, 7)
            oldPair.second = model.takeItem(row, 8)

            #Set items to new value
            model.setItem(row, 7, device) 
            model.setItem(row, 8, attr)

            #Create an index of cells that were changed
            indexPair.first = model.createIndex(row, 7, model) 
            indexPair.second = model.createIndex(row, 8, model)  

            #Grab the items at the locations that were changed
            currentPair.first = model.item(row, 7)
            currentPair.second = model.item(row, 8)

            #Generate list of lists of lists for return
            changedCells[0].append(indexPair)
            changedCells[1].append(oldPair)
            changedCells[2].append(currentPair)

        return changedCells
        
        
class attributeList(QtWidgets.QTableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.moveMode = False
        self.setSelectionBehavior(self.SelectRows) #Select whole rows
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setDragEnabled(False)
        self.setAlternatingRowColors(True)
        
        model = QtGui.QStandardItemModel()
        model.setHorizontalHeaderLabels(['Attribute'])

        self.setModel(model)

        self.horizontalHeader().setStretchLastSection(True)
    
    def DeleteAllRows(self):
        model = self.model()
        model.setRowCount(0)
        

    def AddRow(self,text):
        model = self.model()
        data = []
        item = QtGui.QStandardItem(str(text))
        item.setEnabled(True)
        item.setDropEnabled(False)
        data.append(item)       
        model.appendRow(data)

  


class moveCopyMode(QtWidgets.QPushButton):
    def __init__(self,parent=None):
        super().__init__(parent)

        self.CopyMode = False

    def clicktext(self):
        if self.CopyMode:

            self.CopyMode = False
            self.setText("Copy")
        
        else:
            self.CopyMode = True
            self.setText("Move")

class customStyle(QtWidgets.QProxyStyle):
    def drawPrimitive(self, element, option, painter, widget=None):
        """
        Draw a line across the entire row rather than just the column
        we're hovering over.  This may not always work depending on global
        style - for instance I think it won't work on OSX.
        """
        if element == self.PE_IndicatorItemViewItemDrop and not option.rect.isNull():
            option_new = QtWidgets.QStyleOption(option)
            option_new.rect.setLeft(0)
            if widget:
                option_new.rect.setRight(widget.width())
            option = option_new
        super().drawPrimitive(element, option, painter, widget)

class Ui_MainSpringGUI(QWidget):
    
    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.recipeSteps = recipeSteps(MainSpringGUI)        
        self.allAttr = []


    def setupUi(self, MainSpringGUI):
        MainSpringGUI.setObjectName("MainSpringGUI")
        MainSpringGUI.setEnabled(True)
        #self.recipeSteps = recipeSteps(MainSpringGUI)
        self.recipeSteps.setGeometry(QtCore.QRect(10, 10, 1051, 571))
        self.recipeSteps.setDefaultDropAction(QtCore.Qt.CopyAction)

        #self.DeviceProxyAddress = "raako/handlers/sequencer"  #Matching this to what I put into jive for kicks
        self.DeviceProxyAddress = sys.argv[0]
        testDevice = PyTango.DeviceProxy(sys.argv[1]).read_attribute("State")
        #self.DeviceProxyAddress = "clockmaker/SOW36/Sequencer"
        #self.DeviceProxyAddress = "tango://localhost:10000/sequencer/laptop/demo"
        # add undoview later, this isn't important right this moment

        #self.undoView = QtWidgets.QUndoView(MainSpringGUI)
        #self.undoView.setGeometry(QtCore.QRect(1090, 480, 221, 181))
        #self.undoView.setObjectName("undoView")


        # Adding tabs
        #self.tabWidget = customTabWidget(self)
        #self.setCentralWidget(self.tabWidget)

        self.undoButton = QtWidgets.QPushButton(MainSpringGUI)
        self.undoButton.setGeometry(QtCore.QRect(270, 600, 121, 23))
        self.undoButton.setObjectName("Undo")
        self.undoButton.clicked.connect(proxy.mStack.undo)

        self.redoButton = QtWidgets.QPushButton(MainSpringGUI)
        self.redoButton.setGeometry(QtCore.QRect(270, 630, 121, 23))
        self.redoButton.setObjectName("Redo")
        self.redoButton.clicked.connect(proxy.mStack.redo)

        self.AddStep = QtWidgets.QPushButton(MainSpringGUI)
        self.AddStep.setGeometry(QtCore.QRect(140, 600, 121, 23))
        self.AddStep.setObjectName("AddStep")
        self.AddStep.clicked.connect(self.CriteriaType)
        self.comboBox = QtWidgets.QComboBox(MainSpringGUI)
        self.comboBox.setGeometry(QtCore.QRect(10, 630, 121, 23))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.RemoveStep = QtWidgets.QPushButton(MainSpringGUI)
        self.RemoveStep.setGeometry(QtCore.QRect(140, 630, 121, 23))
        self.RemoveStep.setObjectName("RemoveStep")
        self.RemoveStep.clicked.connect(self.recipeSteps.DeleteRows)

        self.SequencerButton = QtWidgets.QPushButton(MainSpringGUI)
        self.SequencerButton.setGeometry(QtCore.QRect(1090, 630, 221, 23))
        self.SequencerButton.setObjectName("SendToSequencer")
        self.SequencerButton.clicked.connect(self.SendToSequencer)

        self.deviceTreeView = QtWidgets.QTreeWidget(MainSpringGUI)
        self.deviceTreeView.setGeometry(QtCore.QRect(1090, 10, 221, 250))
        self.deviceTreeView.setObjectName("deviceTreeView")
        self.deviceTreeView.setHeaderLabel("Device Servers")
        self.deviceTreeView.setDragEnabled(False)
        self.deviceTreeView.setAlternatingRowColors(True)
        self.buildDeviceTreeView(self.getDeviceServers(),parent=self.deviceTreeView)
        self.deviceTreeView.itemClicked.connect(self.treeClickHandler)
        
        self.attributeListView = attributeList(MainSpringGUI)
        self.attributeListView.setGeometry(1090, 280,221,250)
        self.attributeListView.setDragEnabled(False)
        self.attributeListView.setStyle(customStyle())
        self.attributeListView.clicked.connect(self.attributeListHandler)
        
        self.MoveCopyMode = moveCopyMode(MainSpringGUI)
        self.MoveCopyMode.setGeometry(QtCore.QRect(10, 600, 121, 23))
        self.MoveCopyMode.setAutoDefault(False)
        self.MoveCopyMode.setObjectName("MoveCopyMode")
        self.MoveCopyMode.clicked.connect(self.MoveCopyMode.clicktext)
        self.MoveCopyMode.clicked.connect(self.recipeSteps.moveCopyMode)
        self.Append = QtWidgets.QPushButton(MainSpringGUI)
        self.Append.setGeometry(QtCore.QRect(530, 615, 121, 23))
        self.Append.setAutoDefault(False)
        self.Append.setObjectName("Append")
        self.Append.clicked.connect(self.append)
        self.SaveSteps = QtWidgets.QPushButton(MainSpringGUI)
        self.SaveSteps.setGeometry(QtCore.QRect(400, 600, 121, 23))
        self.SaveSteps.setAutoDefault(False)
        self.SaveSteps.setObjectName("SaveSteps")
        self.SaveSteps.clicked.connect(self.save)
        self.LoadSteps = QtWidgets.QPushButton(MainSpringGUI)
        self.LoadSteps.setGeometry(QtCore.QRect(400, 630, 121, 23))
        self.LoadSteps.setAutoDefault(False)
        self.LoadSteps.setObjectName("LoadSteps")
        self.LoadSteps.clicked.connect(self.open)
        self.AddAttribute = QtWidgets.QPushButton(MainSpringGUI)
        self.AddAttribute.setGeometry(QtCore.QRect(1090, 540,221,23))
        self.AddAttribute.setAutoDefault(False)
        self.AddAttribute.setObjectName("AddAttribute")
        self.AddAttribute.clicked.connect(self.AddAttributeToStep)
        self.AddAttribute.setDisabled(True)
        self.AddCriteria = QtWidgets.QPushButton(MainSpringGUI)
        self.AddCriteria.setGeometry(QtCore.QRect(1090, 570,221,23))
        self.AddCriteria.setAutoDefault(False)
        self.AddCriteria.setObjectName("AddAttribute")
        self.AddCriteria.clicked.connect(self.AddCriteriaToStep)
        self.AddCriteria.setDisabled(True)

        self.SequenceValidator = SequenceValidator.SequenceValidator()

        self.retranslateUi(MainSpringGUI)
        QtCore.QMetaObject.connectSlotsByName(MainSpringGUI)

    def retranslateUi(self, MainSpringGUI):
        _translate = QtCore.QCoreApplication.translate
        MainSpringGUI.setWindowTitle(_translate("MainSpringGUI", "MainSpringGUI"))
        self.AddStep.setText(_translate("MainSpringGUI", "Add Step"))
        self.comboBox.setItemText(0, _translate("MainSpringGUI", "criteria"))
        self.comboBox.setItemText(1, _translate("MainSpringGUI", "parameter"))
        self.comboBox.setItemText(2, _translate("MainSpringGUI", "duration"))
        self.comboBox.setItemText(3, _translate("MainSpringGUI", "jump"))
        self.comboBox.setItemText(4, _translate("MainSpringGUI", "jumpIf"))
        self.comboBox.setItemText(5, _translate("MainSpringGUI", "jumpCompare"))
        self.RemoveStep.setText(_translate("MainSpringGUI", "Remove Step"))
        self.MoveCopyMode.setText(_translate("MainSpringGUI", "Copy"))
        self.Append.setText(_translate("MainSpringGUI", "Append"))
        self.SaveSteps.setText(_translate("MainSpringGUI", "Save"))
        self.LoadSteps.setText(_translate("MainSpringGUI", "Load"))
        self.AddAttribute.setText(_translate("MainSpringGUI", "AddAttribute"))
        self.AddCriteria.setText(_translate("MainSpringGUI", "AddCriteria"))
        self.SequencerButton.setText(_translate("MainSpringGUI", "SendToSequencer"))
        self.undoButton.setText(_translate("MainSpringGUI", "Undo"))
        self.redoButton.setText(_translate("MainSpringGUI", "Redo"))

    def treeClickHandler(self,item,column_number):

        deviceserverpathparts = []
        
        # this basically traverses the tree and grabs all the parents, then reverses it so the text is in the right order.
        while item is not None:
            deviceserverpathparts.append(item.text(0))
            item = item.parent()
        deviceserverpathparts.reverse()

        # generate the full path.
        deviceserverpath = '/'.join(deviceserverpathparts)
        # set for later.
        self.deviceserverpath = deviceserverpath
        
        # if there are 3 parts (ie address conforms with 1/2/3 address), grab attributes.
        if len(deviceserverpathparts)==3:
            attributes = PyTango.DeviceProxy(deviceserverpath).get_attribute_list()
            
            listattr = []
            # remove presently selected attributes.

            self.attributeListView.DeleteAllRows()
            
            # get all attributes and push them to the view in alphabetical order.
            for i in range(len(attributes)):
                listattr.append(str(attributes[i]))
                self.allAttr.append(str(attributes[i]))
            listattr.sort()            
            for i in range(len(listattr)):
                self.attributeListView.AddRow(str(listattr[i]))

    def attributeListHandler(self):
        
        index = self.attributeListView.selectionModel().currentIndex()
        attribute = index.sibling(index.row(),index.column()).data()
        self.attributetext = str(attribute)
        self.AddAttribute.setEnabled(True)
        self.AddCriteria.setEnabled(True)

    def SendToSequencer(self):
        print("Sending to Sequencer")
        try:
            tableDict = self.recipeSteps.DumpSteps()
            tableString = str(json.dumps(tableDict))
            validationcode, validationstring = self.SequenceValidator.ValidateSequence(tableString)
            print(validationcode)
            if validationcode >=0:
                SequencerDS = PyTango.DeviceProxy(self.DeviceProxyAddress)
                SequencerDS.LoadSequence(tableString)
                print("Loaded sequence")
            else:
                msg = QMessageBox()
                msg.setWindowTitle("Error!")
                msg.setText("Validation failed with code " + str(validationcode) + "\r\n" + str(validationstring))
                x = msg.exec_()
        except:
            traceback.print_exc()

    def AddAttributeToStep(self):
        command = commandAddAttributeToStep()
        proxy.mStack.push(command)

    def AddCriteriaToStep(self):
        command = commandAddCriteriaToStep()
        proxy.mStack.push(command)

    def CriteriaType(self):
        
        # sets the criteria based on the incoming value.

        command = commandAddStep()
        proxy.mStack.push(command)

    def open(self,append=True): #was: False
        
        path = QtWidgets.QFileDialog.getOpenFileName(self, 'Open a file', os.path.expanduser('~'),'All Files (*.*)')
        
        if path != ('', ''):
            
            
            file = open(path[0])
            validationstep = False
            try:
                jsondata = json.load(file)
                validationstep = True
                validationcode, validationstring = self.SequenceValidator.ValidateSequence(json.dumps(jsondata))

            except:
                msg = QMessageBox()
                msg.setWindowTitle("Error!")
                if validationstep:
                    msg.setText("Validation failed with code " + str(validationcode) + "\r\n" + str(validationstring))
                else:
                    msg.setText("Bad JSON on load! File is corrupt.")
                x = msg.exec_()
            if not append:
                self.recipeSteps.DeleteAllRows()
            data = self.recipeSteps.PopulateJSON(jsondata, False)
            return data

        
    
    def append(self):
        
        command = commandAppend()
        proxy.mStack.push(command)
            
        
    def save(self):

        tableDict = self.recipeSteps.DumpSteps()
        validationcode, validationstring = self.SequenceValidator.ValidateSequence(str(json.dumps(tableDict)))
        if validationcode >=0:
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.DontUseNativeDialog
            fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self,"Save Sequencer File",os.path.expanduser('~'),"JSON Files (*.json)", options=options)
            if fileName:
                if ".json" not in fileName:
                    fileName = fileName + ".json"
                print(fileName)
                with open(fileName,'w') as outputfile:
                    json.dump(tableDict,outputfile)
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Error!")
            msg.setText("Validation failed with code " + str(validationcode) + "\r\n" + str(validationstring))
            x = msg.exec_()



    def getDeviceServers(self):
        
        # grab exported device servers.
        deviceservers =  deviceservers = PyTango.Database().get_device_exported('*').value_string     
        # sort into a nested dict.
        tree = {} 
        # for each path
        for path in deviceservers:                
            # start at the top
            node = tree       
            # split into a list
            for level in path.split('/'): 
                # split the path into a list 
                if level:                 
                    # if a name is non-empty, move to the deeper level or create if nonexistent
                    node = node.setdefault(level, dict()) 

        
        return tree
        
    def buildDeviceTreeView(self,data=None, parent=None):

        for key,value in data.items():

            item = QTreeWidgetItem(parent)
            item.setText(0,key)
            if isinstance(value,dict):
                self.buildDeviceTreeView(data=value,parent=item)

class ComboBoxItemDelegate(QStyledItemDelegate):
    # Code adapted from https://wiki.qt.io/Combo_Boxes_in_Item_Views
    def createEditor(self, parent: QWidget, option: QStyleOptionViewItem, index: QModelIndex):
        super().createEditor(parent, option, index)
        cb = QComboBox(parent)
        cb.addItem("equals")
        cb.addItem("notEquals")
        cb.addItem("greaterThan")
        cb.addItem("lessThan")
        return cb    

class UndoRedoProxy(QSortFilterProxyModel):
    def __init__(self, parent = None):
        super().__init__(parent)

        self.mStack = QUndoStack()

        self.undoAction = self.mStack.createUndoAction(self, self.tr("&Undo"))        
        self.redoAction = self.mStack.createRedoAction(self, self.tr("&Redo"))
        
        
class commandSetData(QUndoCommand):
    def __init__(self, model, index, newData, oldData):
        super().__init__()
        self.index = index
        self.newData = newData
        self.oldData = oldData
        self.model = ui.recipeSteps.model()

    def undo(self):
        print("Undoing cell change")
        QStandardItemModel.setData(self.model, self.index, self.oldData, Qt.ItemDataRole.EditRole) #Set data at index to the previous value

    def redo(self):
        print("Redoing cell change")
        QStandardItemModel.setData(self.model, self.index, self.newData, Qt.ItemDataRole.EditRole) #Set data at index to the new value


class commandAddStep(QUndoCommand):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.mIndex = ui.recipeSteps.modelmodel.pathFromIndex(ui.recipeSteps.modelmodel.createIndex(ui.recipeSteps.modelmodel.rowCount(),0))
        
        self.data = None
        self.model = ui.recipeSteps.model()
        self.oldRowIndex = None
        self.oldRowPtr = None #QStandardItem list
        self.redoing = False

    def redo(self):
        if not self.redoing:
            print("doing AddStep")
            ui.recipeSteps.stepType = str(ui.comboBox.currentText()) #This should match the text at the time of the first redo call i.e. the "do"
            ui.recipeSteps.AddRow()
        
        if self.redoing:
            print("redoing AddStep")
            ui.recipeSteps.AddRow()
    
    def undo(self):
        print("undoing AddStep")
        self.oldRowIndex = ui.recipeSteps.modelmodel.pathToIndex(self.mIndex, ui.recipeSteps.modelmodel)
        model = ui.recipeSteps.model()
        model.removeRow(self.oldRowIndex.row()) #Deleting the row is OK here since it will never have a populated cell at the time of undo

        self.redoing = True



class commandDeleteRows(QUndoCommand):
    def __init__(self, parent=None):
        print("initializing commandDeleteRows")
        super().__init__(parent)
        self.index_list = []
        self.model = ui.recipeSteps.model()
        self.oldRowIndex = None
        self.oldRowPtr = None
        self.oldRows = []
        self.redoing = False

        self.rowList = []
        self.reorderedIndex = []


    def redo(self):
        print("redoing DeleteRows")

        if self.redoing:
            for rows in reversed(range(len(self.rowList))): 
                self.oldRowPtr = ui.recipeSteps.modelmodel.takeRow(self.rowList[rows])

        if not self.redoing:

            self.model = ui.recipeSteps.model()
            index_list = []
            for model_index in ui.recipeSteps.selectionModel().selectedRows():
                index = QtCore.QPersistentModelIndex(model_index)
                index_list.append(index)

            for row in range(len(index_list)): #creates a list of row numbers in the order that they were selected
                self.rowList.append(index_list[row].row())
            
            for index in index_list:
                templist = []
                #print(index.row()) placed here, prints correct row. If it is placed after the takeRow, returns invalid row.
                self.oldRowIndex = self.model.createIndex(index.row(),index.column())
                self.oldRowPtr = ui.recipeSteps.modelmodel.takeRow(index.row())
                templist.append(self.oldRowPtr)
                self.oldRows.append(templist) #double append here nets a list of lists        

    def undo(self):
        print("undoing DeleteRows")

        self.redoing = True

        for rows in range(len(self.rowList)):
            self.model.insertRow(self.rowList[rows]) 
            
        for rows in range(len(self.rowList)):
            for col in range(self.model.columnCount()):
                self.model.setItem(self.rowList[rows], col, self.oldRows[rows][0][col])

                
class commandAppend(QUndoCommand):

    def __init__(self, parent = None):
        super().__init__(parent)
        self.data = None
    
    def redo(self):
        print("redoing append")
        self.data = ui.open()
        

    def undo(self):
        print("undoing append")
        model = ui.recipeSteps.model()
        for i in range(len(self.data)):
            model.removeRow(model.rowCount()-1)


class commandAddAttributeToStep(QUndoCommand):
    def __init__(self, parent = None):
        super().__init__(parent)
        print("Initializing commandAddAttributeToStep")
        self.redoing = False
        self.model = ui.recipeSteps.model()
        self.cellData = None
        self.currentValue = None
        self.currentItemFirst = None
        self.currentItemSecond = None
        self.oldValuesList = []
        
    
    def redo(self):
        if self.redoing:
            print("redoing adding attribute to step")
            for i in range(len(self.cellData[1])):
                self.oldValues = QPair()
                row = self.cellData[0][i].first.row()
                currentItemFirst = self.cellData[2][i].first
                currentItemSecond = self.cellData[2][i].second
                self.model.setItem(row, 3, currentItemFirst)
                self.model.setItem(row, 4, currentItemSecond)

                self.oldValues.first = currentItemFirst.clone()
                self.oldValues.second = currentItemSecond.clone()

                self.oldValuesList.append(self.oldValues)

            self.redone = True

        elif not self.redoing:
            print("doing adding attribute to step")
            self.cellData = ui.recipeSteps.AddAddressAndAttribute(ui.deviceserverpath,ui.attributetext)    
             
    def undo(self):
        print("undoing adding attribute to step")
        self.redoing = True

        for i in range(len(self.cellData[1])):
            role = Qt.ItemDataRole.EditRole
            
            indexFourthColumn = self.cellData[0][i].first
            indexFifthColumn = self.cellData[0][i].second

            row = indexFourthColumn.row()

            #Take the current items before they are deleted to facilitate redo
            self.cellData[2][i].first = self.model.takeItem(row, 3)
            self.cellData[2][i].second = self.model.takeItem(row, 4)
            
            self.oldValueFourthColumn = self.cellData[1][i].first
            self.oldValueFifthColumn = self.cellData[1][i].second
            self.model.setItem(row, 3, QStandardItem(str(self.oldValueFourthColumn.data(Qt.ItemDataRole.EditRole))))
            self.model.setItem(row, 4, QStandardItem(str(self.oldValueFifthColumn.data(Qt.ItemDataRole.EditRole))))


class commandAddCriteriaToStep(QUndoCommand):
    def __init__(self, parent = None):
        super().__init__(parent)
        print("Initializing commandAddAttributeToStep")
        self.redoing = False
        self.model = ui.recipeSteps.model()
        self.cellData = None
        self.currentValue = None
        self.currentItemFirst = None
        self.currentItemSecond = None
        self.oldValuesList = []
        
    
    def redo(self):
        if self.redoing:
            print("redoing adding attribute to step")
            for i in range(len(self.cellData[1])):
                self.oldValues = QPair()
                row = self.cellData[0][i].first.row()
                currentItemFirst = self.cellData[2][i].first
                currentItemSecond = self.cellData[2][i].second
                self.model.setItem(row, 7, currentItemFirst)
                self.model.setItem(row, 8, currentItemSecond)

                self.oldValues.first = currentItemFirst.clone()
                self.oldValues.second = currentItemSecond.clone()

                self.oldValuesList.append(self.oldValues)

            self.redone = True
            

        elif not self.redoing:
            print("doing adding attribute to step")
            self.cellData = ui.recipeSteps.AddCompareAddressAndAttribute(ui.deviceserverpath,ui.attributetext)    
             
    def undo(self):
        print("undoing adding attribute to step")
        self.redoing = True

        for i in range(len(self.cellData[1])):
            role = Qt.ItemDataRole.EditRole
            
            indexFourthColumn = self.cellData[0][i].first
            indexFifthColumn = self.cellData[0][i].second

            row = indexFourthColumn.row()

            #Take the current items before they are deleted to facilitate redo
            self.cellData[2][i].first = self.model.takeItem(row, 7)
            self.cellData[2][i].second = self.model.takeItem(row, 8)
            
            self.oldValueFourthColumn = self.cellData[1][i].first
            self.oldValueFifthColumn = self.cellData[1][i].second
            self.model.setItem(row, 7, QStandardItem(str(self.oldValueFourthColumn.data(Qt.ItemDataRole.EditRole))))
            self.model.setItem(row, 8, QStandardItem(str(self.oldValueFifthColumn.data(Qt.ItemDataRole.EditRole))))     
        

class commandDropMimeData(QUndoCommand): 
    def __init__(self, data, action, row, col, parent):
        super().__init__()
        self.data = data
        self.action = action
        self.row = row
        self.column = col
        self.parent = parent
        self.model = ui.recipeSteps.model()
        self.oldRows = [[]] #List of lists containing row contents in form of QStandardItems
        self.uniqueRows = [] #Stores the original locations of drag and dropped rows
        self.indexes = self.model.indexesMimed
        self.redoing = False
    
    def redo(self):
        if not self.redoing:
            print("doing drop MIME data")
            QStandardItemModel.dropMimeData(ui.recipeSteps.modelmodel,self.data, Qt.CopyAction, self.row, 0, self.parent)
            for i in range(int(self.model.rowCount())):
                
                item = self.model.item(i,1)
                item.setEnabled(False) # Hack / Fixes copying instead of moving when style column is disabled

        if self.redoing: 
            print("redoing drop MIME data")
            
            if self.model.moveMode:
                for i in reversed(range(len(self.uniqueRows))):
                    row = self.uniqueRows[i]
                    self.oldRows[i] = self.model.takeRow(row)
                
            for i in reversed(range(len(self.uniqueRows))):
                row = self.row
                self.model.insertRow(row, self.oldRows[i])


    def undo(self):

        print("undoing drop MIME data")
     
        if not self.redoing:
            for i in range(len(self.indexes)):
                if i == 0: 
                    self.uniqueRows.append(self.indexes[i].row()) # Add the first row value to the list
                elif self.indexes[i].row() != self.indexes[i-1].row(): # If the row value is different from the row value before it, add it to the list
                        self.uniqueRows.append(self.indexes[i].row())
                        self.uniqueRows.sort() # Sort in ascending order to ensure correctly ordered row insertion      

            for i in range(len(self.uniqueRows)): #For every unique row number, take the row in question and append to oldRow
                self.oldRows.append(self.model.takeRow(self.row)) 
                if i == 0:
                    self.oldRows.pop(0) #Remove errant empty list at position 0 after the first instance of append

            ##If moveCopyMode is move, insert the oldRows at the old locations and set the items accordingly.

            if self.model.moveMode:
                for i in range(len(self.uniqueRows)):
                    row = self.uniqueRows[i]
                    contents = self.oldRows[i]
                    self.model.insertRow(row, contents)
        
        if self.redoing:

            if not self.model.moveMode:
                for i in range(len(self.uniqueRows)):
                    self.model.takeRow(self.row)

                
            if self.model.moveMode:
                for i in range(len(self.uniqueRows)): #For every unique row number, take the row in question and write to oldRow
                    self.oldRows[i] = self.model.takeRow(self.row)

                for i in range(len(self.uniqueRows)):
                    row = self.uniqueRows[i]
                    contents = self.oldRows[i]
                    self.model.insertRow(row, contents)

        self.redoing = True
      
class customTabWidget(QWidget):
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.layout = QVBoxLayout(self)
        self.tabs = QTabWidget() 
        self.tab1 = QWidget() 
        self.tab2 = QWidget() 
        self.tabs.resize(400,400)
        # Add tabs 
        self.tabs.addTab(self.tab1, "Geeks") 
        self.tabs.addTab(self.tab2, "For") 

class QPair():
    def __init__(self, parent = None):
        super().__init__()
        self.first = 0
        self.second = 0   


if __name__ == "__main__":
   
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainSpringGUI = QtWidgets.QWidget()
    proxy = UndoRedoProxy()
    ui = Ui_MainSpringGUI()
    ui.setupUi(MainSpringGUI)
    MainSpringGUI.show()
    sys.exit(app.exec_())

